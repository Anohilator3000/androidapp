package com.example.myfirstandroidapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.*;
import java.lang.*;
import java.io.*;


public class MainActivity extends AppCompatActivity {

    private TextView plList;
    private TextView ErrLog;
    private Button Btn1;
    private Button Btn2;
    private Button Btn3;
    private Button Btn4;
    private Button Btn5;
    private Button Btn6;
    private Button Btn7;
    private Button Btn8;
    private Button Btn9;
    private Button Btn0;
    private Button BtnOBr;
    private Button BtnCbr;
    private Button BtnPlus;
    private Button BtnMinus;
    private Button BtnMul;
    private Button BtnDiv;
    private Button BtnRes;
    private Button BtnClean;
    private Button BtnCorr;

    private static String operators = "+-*/";
    private static String delimiters = "() " + operators;
    public static boolean flag = true;
    private static boolean isDelimiter(String token) {
        if (token.length() != 1) return false;
        for (int i = 0; i < delimiters.length(); i++) {
            if (token.charAt(0) == delimiters.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isOperator(String token) {
        if (token.equals("u-")) return true;
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isFunction(String token) {
        if (token.equals("sqrt") || token.equals("cube") || token.equals("pow10")) return true;
        return false;
    }

    private static int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    public List<String> parse(String infix) {
        List<String> postfix = new ArrayList<String>();
        Deque<String> stack = new ArrayDeque<String>();
        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
        String prev = "";
        String curr = "";
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                ErrLog.setText("Некорректное выражение.");
                flag = false;
                return postfix;
            }
            if (curr.equals(" ")) continue;
            if (isFunction(curr)) stack.push(curr);
            else if (isDelimiter(curr)) {
                if (curr.equals("(")) stack.push(curr);
                else if (curr.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        postfix.add(stack.pop());
                        if (stack.isEmpty()) {
                            ErrLog.setText("Скобки не согласованы.");
                            flag = false;
                            return postfix;
                        }
                    }
                    stack.pop();
                    if (!stack.isEmpty() && isFunction(stack.peek())) {
                        postfix.add(stack.pop());
                    }
                }
                else {
                    if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev)  && !prev.equals(")")))) {
                        // унарный минус
                        curr = "u-";
                    }
                    else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }

                    }
                    stack.push(curr);
                }

            }

            else {
                postfix.add(curr);
            }
            prev = curr;
        }

        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) postfix.add(stack.pop());
            else {
                //System.out.println();
                ErrLog.setText("Скобки не согласованы.");
                flag = false;
                return postfix;
            }
        }
        return postfix;
    }

    public static Double calc(List<String> postfix) {
        Deque<Double> stack = new ArrayDeque<Double>();
        for (String x : postfix) {
            if (x.equals("sqrt")) stack.push(Math.sqrt(stack.pop()));
            else if (x.equals("cube")) {
                Double tmp = stack.pop();
                stack.push(tmp * tmp * tmp);
            }
            else if (x.equals("pow10")) stack.push(Math.pow(10, stack.pop()));
            else if (x.equals("+")) stack.push(stack.pop() + stack.pop());
            else if (x.equals("-")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a - b);
            }
            else if (x.equals("*")) stack.push(stack.pop() * stack.pop());
            else if (x.equals("/")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a / b);
            }
            else if (x.equals("u-")) stack.push(-stack.pop());
            else stack.push(Double.valueOf(x));
        }
        return stack.pop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        plList = findViewById(R.id.tv_pl_names);
        ErrLog = findViewById(R.id.tv_pl_info);
        Btn1 = findViewById(R.id.btn_num_1);
        Btn2 = findViewById(R.id.btn_num_2);
        Btn3 = findViewById(R.id.btn_num_3);
        Btn4 = findViewById(R.id.btn_num_4);
        Btn5 = findViewById(R.id.btn_num_5);
        Btn6 = findViewById(R.id.btn_num_6);
        Btn7 = findViewById(R.id.btn_num_7);
        Btn8 = findViewById(R.id.btn_num_8);
        Btn9 = findViewById(R.id.btn_num_9);
        Btn0 = findViewById(R.id.btn_num_0);
        BtnOBr = findViewById(R.id.btn_num_open_br);
        BtnCbr = findViewById(R.id.btn_num_close_br);
        BtnPlus = findViewById(R.id.btn_num_plus);
        BtnMinus = findViewById(R.id.btn_num_minus);
        BtnMul = findViewById(R.id.btn_num_mul);
        BtnDiv = findViewById(R.id.btn_num_div);
        BtnRes = findViewById(R.id.btn_num_result);
        BtnClean = findViewById(R.id.btn_num_clean);
        BtnCorr = findViewById(R.id.btn_num_corr);

        View.OnClickListener oclBtnBtn = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ErrLog.setText("");
                if (v.getId() == R.id.btn_num_clean) {
                    plList.setText("");
                }
                else {
                    if(v.getId() == R.id.btn_num_corr) {
                        String  CorrText = plList.getText().toString();
                        if(CorrText.length()>0) {
                            CorrText = CorrText.substring(0, CorrText.length() - 1);

                            plList.setText(CorrText);
                        }
                    }
                    else {
                        if(v.getId() == R.id.btn_num_result){
                            String s = plList.getText().toString();
                            flag = true;
                            List<String> expression = parse(s);
                            if (flag) {
                                //for (String x : expression) System.out.print(x + " ");
                                //System.out.println();
                                Double YouToo = calc(expression);
                                String result = (YouToo.toString());
                                plList.setText(result);
                            }
                        }
                        else{
                            Button b = (Button)v;
                            plList.append(b.getText().toString());
                        }
                    }
                }

            }



        };
        Btn1.setOnClickListener(oclBtnBtn);
        Btn2.setOnClickListener(oclBtnBtn);
        Btn3.setOnClickListener(oclBtnBtn);
        Btn4.setOnClickListener(oclBtnBtn);
        Btn5.setOnClickListener(oclBtnBtn);
        Btn6.setOnClickListener(oclBtnBtn);
        Btn7.setOnClickListener(oclBtnBtn);
        Btn8.setOnClickListener(oclBtnBtn);
        Btn9.setOnClickListener(oclBtnBtn);
        Btn0.setOnClickListener(oclBtnBtn);
        BtnOBr.setOnClickListener(oclBtnBtn);
        BtnCbr.setOnClickListener(oclBtnBtn);
        BtnPlus.setOnClickListener(oclBtnBtn);
        BtnMinus.setOnClickListener(oclBtnBtn);
        BtnMul.setOnClickListener(oclBtnBtn);
        BtnDiv.setOnClickListener(oclBtnBtn);
        BtnRes.setOnClickListener(oclBtnBtn);
        BtnClean.setOnClickListener(oclBtnBtn);
        BtnCorr.setOnClickListener(oclBtnBtn);

    }
}